
/*
Create a menu item for each installed search engine.
The ID and title are both set to the search engine's name.
*/
const InitMenuId = 'd598e92e'
function createMenuItems(engines) {
  for (let engine of engines) {
    if (!engine.checked) continue;
    chrome.contextMenus.create({
      id: engine.keyword,
      title: engine.name,
      contexts: ["selection"],
      parentId: InitMenuId
    });
  }
}

const engines = JSON.parse(localStorage.getItem('engines') || '[]')
const parentItem = localStorage.getItem('pname') || '搜索'

function getEngines() {
  return engines
}

function reloadAll() {
  chrome.contextMenus.removeAll()
  InitMenu()
  localStorage.setItem("engines", JSON.stringify(engines))
}

function addOne(idx) {
  const engine = engines[idx]
  chrome.contextMenus.create({
    id: engine.keyword,
    title: engine.name,
    contexts: ["selection"],
    parentId: InitMenuId
  });
  localStorage.setItem("engines", JSON.stringify(engines))

}
function InitMenu() {
  chrome.contextMenus.create({
    id: InitMenuId,
    title: parentItem,
    contexts: ["selection"]
  });
  createMenuItems(engines)
}

InitMenu()
/*
Search using the search engine whose name matches the
menu item's ID.
*/
chrome.contextMenus.onClicked.addListener((info, tab) => {
  if (info.menuItemId === InitMenuId) return;
  const engine = engines.find((v) => v.keyword === info.menuItemId)
  chrome.tabs.create({
    url: engine?.url.replace("%s", info.selectionText),
    index: tab.index + 1
  });
});
// chrome.tabs.onCreated.addListener(( /**@type {number}*/tabId, changeInfo, tab) => {
//   debugger
// })
// chrome.tabs.onUpdated.addListener(( /**@type {number}*/tabId, changeInfo, tab) => {
//   debugger
// })