
const getEngine = new Promise((resolve, reject) => {
    chrome.storage.local.get('engines', (items) => {
        resolve(items)
    })
})

getEngine.then((items) => {
    if (items.engines) {

        let i = -1;
        const roothtml = `
        <style>
        #search-box-4589.show4589 {
            display:flex;
        }
        #search-box-4589.hide4589 {
            display:none;
        }
        #search-box-4589 {
            font-size: 14px;
            cursor: pointer;
            user-select: none;
            position: absolute;
            border-radius: 50px;
            display:none;
            color: #00000047;
            background-color: #ffffff55;
            z-index: 9999999;
            padding-right: 5px;
            align-items: center;
        }
        #search-box-4589:hover {
            display:flex;
            color: black;
            background-color: white;
            box-shadow: #00000096 0px 0px 4px 0px;
        }

        #search-box-4589 > tdiv {
            padding: 15px;
        }
        #search-box-4589:hover > sdiv {
            display: block;
        }
        #search-box-4589>sdiv {
            display: none;
            padding: 15px 5px 15px 22px;
            // border-left: 1px black dashed;
            background-repeat: no-repeat;
            background-size: 16px;
            background-position: 2px center;
        }
        #search-box-4589>sdiv:hover {
            text-decoration: underline;
            background-color: aliceblue;
        }
        #search-box-4589>sdiv:last-child {
            border-radius: 0 50px 50px 0;
        }

        #search-box-4589>sdiv:active {
            background-color: #e0f1ff;

        }
    </style>
    <sdiv id="search-box-4589">
        <tdiv data-idx=0">[x]</tdiv>
`
            + items.engines.reduce((acc, cur) => {
                i++;
                const ma = cur.url.match(/^https?:\/\/([^/]+)/)
                const imgURL = chrome.runtime.getURL("icons/" + ma[1] + ".ico");

                return acc + `<sdiv data-idx="${i}" style="background-image:url(${imgURL})">${cur.name}</sdiv>`
            }, "") +
            `  </sdiv>

        `
        document.body.insertAdjacentHTML('beforeend', roothtml)
        const searchBox = document.querySelector('#search-box-4589')

        searchBox.onclick = (e) => {
            const idx = e.target.dataset.idx
            if (idx !== undefined) {
                const targetUrl = items.engines[parseInt(idx)].url.replace('%s', lastSelect)
                window.open(targetUrl, '_blank')
            }
        }
        let lastSelect = '';
        document.addEventListener('selectionchange', (e) => {
            lastSelect = document.getSelection().toString()
        })
        let pendingClose = []
        window.addEventListener('pointerup', (e) => {
            if (e.button == 0 && lastSelect.length > 0) {
                setTimeout(() => {
                    if (lastSelect.length === 0) {
                        searchBox.classList.toggle('show4589', false)
                        return
                    }
                    searchBox.style.left = -13 + e.pageX + 'px'
                    searchBox.style.top = -57 + e.pageY + 'px'
                    searchBox.classList.toggle('show4589', true)
                    if (pendingClose.length === 0) {
                        setTimeout(() => {
                            pendingClose.length = 0
                            searchBox.classList.toggle('show4589', false)
                        }, 2000)

                    }
                }, 200)

            }

        })
    }
})


