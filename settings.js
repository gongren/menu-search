const $ = document.querySelector.bind(document)
const bp = chrome.extension.getBackgroundPage();
const engines = bp.getEngines()

const engineLis = $('#enginelist')

function updateList() {
    engines.forEach((v, i) => {
        engineLis.insertAdjacentHTML("beforeend",
            `<div><input type="checkbox" data-index="${i}" ${v.checked ? 'checked' : ''} data-keyword="${v.keyword}">
            <span>${v.name}</span>
            <span>${v.keyword}</span>
            <span>${v.url}</span></div>`
        )
    })
}
updateList()

engineLis.onchange = (e) => {
    const idx = e.target.dataset.index
    if (engines[idx].checked !== e.target.checked) {
        engines[idx].checked = e.target.checked
        bp.reloadAll()
    }
}
const addfrom = $("#addform")
const textall = $('#textall')
const textallsave = $('#textallsave')
const texalltarea = $('#texalltarea')

$('#editall').onclick = () => {
    texalltarea.value = JSON.stringify(engines)
    textall.style.display = "block"
}
textallsave.onclick = () => {
    textall.style.display = "none"
    const v = JSON.parse(texalltarea.value)
    engines.splice(0, engines.length)
    engines.push(...v)
    bp.reloadAll()
    engineLis.innerHTML = ''
    updateList()
    chrome.storage.local.set({ engines })

}


$("#add").onclick = () => {
    addfrom.style.display = "block"
    addfrom.querySelectorAll('input').forEach(value => {
        value.value = ''
    })
}
$("#save").onclick = () => {
    addfrom.style.display = "none"
    const v = {}
    for (const value of addfrom.querySelectorAll('input')) {
        if (value.value.trim().length === 0) return;
        v[value.name] = value.value
    }
    v.checked = true
    engines.push(v)
    const i = engines.length - 1
    bp.addOne(i)


    engineLis.insertAdjacentHTML("beforeend",
        `<div><input type="checkbox" data-index="${i}" ${v.checked ? 'checked' : ''} data-keyword="${v.keyword}">
    <span>${v.name}</span>
    <span>${v.keyword}</span>
    <span>${v.url}</span></div>`
    )

}
